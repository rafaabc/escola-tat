/// <reference types="cypress" />
describe('Tickets', () => {
  beforeEach(() => cy.visit('https://bit.ly/2XSuwCW'));

  it("Fills all the text input fields", () => {
    const firstName = 'Rafael';
    const lastName = 'Costa';

    cy.get('#first-name').type(firstName);
    cy.get('#last-name').type(lastName);
    cy.get('#email').type('teste@teste.com.br');
    cy.get('#requests').type('none');
    cy.get('#signature').type(`${firstName} ${lastName}`);    
  });

  it('Select two tickets', () => {
    cy.get('#ticket-quantity').select('2');
  });

  it("Select 'VIP' ticket type", () => {
    cy.get('#vip').check();
  });

  it("Selects 'social media' checkbox", () => {
    cy.get('#social-media').check();
  });

  it("Selects 'friend', and 'publication', then uncheck 'friend'", () => {
    cy.get('#friend').check();
    cy.get('#publication').check();
    cy.get('#friend').uncheck();
  });
  it("Has 'TICKETBOX' header's heading", () => {
    cy.get('header h1').should('contain', 'TICKETBOX');
  });  

  it('Alerts on invalid email', () => {
    cy.get('#email')
      .as('email')
      .type('teste-teste.com.br');

    cy.get('#email.invalid').should('exist');

    cy.get('#email')
      .clear()
      .type('teste@teste.com.br');

    cy.get('#email.invalid').should('not.exist');
  });

  it('Fills and reset forme', () => {
    const firstName = 'Rafael';
    const lastName = 'Costa';
    const fullName = `${firstName} ${lastName}`;

    cy.get('#first-name').type(firstName);
    cy.get('#last-name').type(lastName);
    cy.get('#email').type('teste@teste.com.br');
    cy.get('#ticket-quantity').select('2');
    cy.get('#vip').check();
    cy.get('#friend').check();
    cy.get('#requests').type('Wheat beer');

    cy.get('.agreement p').should(
      'contain', `I, ${fullName}, wish to buy 2 VIP tickets.`
    );

    cy.get('#agree').click();
    cy.get('#signature').type(fullName);

    cy.get("button[type='submit']")
    .as('submitButton')
    .should('not.be.disabled');

    cy.get("button[type='reset']").click();

    cy.get('@submitButton').should('be.disabled');
  });

  it('Fills mandatory fields using support command', () => {
    const customer = {
      firstName: 'Rafael',
      lastName: 'Costa',
      email: 'teste@teste.com'
    };

    cy.fillMandatoryFields(customer);

    cy.get("button[type='submit']")
    .as('submitButton')
    .should('not.be.disabled');

    cy.get('#agree').uncheck();

    cy.get('@submitButton').should('be.disabled');
  });
});